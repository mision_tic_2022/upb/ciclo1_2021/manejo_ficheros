#Importar biblioteca json para manipulación de ficheros json
import json

"""
CREAR O SOBREESCRIBIR UN ARCHIVO JSON
"""
placa: str = "kjr-545"
mi_diccionario: dict = {
    placa: {
        "marca": "BMW",
        "color": "negro"
    }
}

try:
    #Crear un fichero o sobreescribir
    with open("bd_carros.json","w") as archivo:
        #Escribir un archivo json
        json.dump(mi_diccionario, archivo)
except:
    print("Error al guardar los datos")

"""
LEER UN FICHERO JSON
"""
info_carros: dict = {}
try:
    with open("bd_carros.json") as archivo:
        info_carros =  json.load(archivo) 
except:
    print("Error al cargar los datos")
    info_carros = {}

print(info_carros)

"""
1 -> Crear carro
2 -> Consultar carros
    -------krj545------
    Color: negro
    Marca: BMW
    -------------------
3 -> Actualizar
0 -> Salir
"""

def mostrar_datos(diccionario_carros: dict):
    for clave, valor in diccionario_carros.items():
        valor["color"]
        valor["marca"]