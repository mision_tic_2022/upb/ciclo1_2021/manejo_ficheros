import json
"""
{
    '01:{
            "nombre": "Andres",
            "apellido": "Quintero",
            "edad": 27
        },
    '02': {
        ...
    }
}

1 -> Consultar estudiantes
2 -> Crear estudiante
{
    "nombre": "Andres",
    "apellido": "Quintero",
    "edad": 27,
    "nombre": "ana"
}
"""

def vista():
    estudiantes = cargar_datos()
    llaves = estudiantes.keys()
    cont: int = len(llaves) + 1
    opc: int = -1
    while opc != 0:
        print("1 --> Consultar estudiantes")
        print("2 --> Crear estudiante")
        print("2 --> Actualizar estudiante")
        print("2 --> Eliminar estudiante")
        print("0 --> Salir")
        opc = int( input("Por favor ingrese una opción: ") )
        controlador(opc, cont, estudiantes)
        cont += 1
"""
list comprehention
numeros = [1,2,3,4,5]
lista = [ n for n in numeros if n%2 == 0 ]
"""
def controlador(opc: int, cont: int, estudiantes: dict):
    #Condicionales
    if opc == 1:
        #COnsultar los estudiantes
        print(cargar_datos() )
    elif opc == 2:
        #Crear un estudiante
        nombre: str = input("Nombre del estudiante: ")
        apellido: str = input("Apellido del estudiante: ")
        edad: int = int(input("Edad del estudiante: "))
        cedula: str = input("Cédula del estudiante: ")
        nuevo_estudiante: dict ={
            "nombre": nombre,
            "apellido": apellido,
            "edad": edad,
            "cedula": cedula
        }
        estudiantes[str(cont)] = nuevo_estudiante
        guardar_datos(estudiantes)
    elif opc == 3:
        pass

def cargar_datos():
    #Creamos diccionario vacio
    estudiantes: dict = dict()
    try:
        #Cargamos la información del json
        with open("datos_estudiantes.json") as archivo:
            estudiantes = json.load(archivo)
    except:
        print("Error al cargar los datos")
    return estudiantes


def guardar_datos(estudiante: dict):
    with open("datos_estudiantes.json", "w") as archivo:
        json.dump(estudiante, archivo)
        print("ESTUDIANTE GUARDADO CON ÉXITO!")

def update(estudiante: dict):
    pass

def delete():
    pass

vista()