#Librería que me permite manipular archivos json
import json

datos_persona = {}
#Lectura de un fichero JSON
with open("datos.json") as archivo:
    #Carga la información del Json
    datos_persona = json.load(archivo)
    print(datos_persona)

#Creamos una nueva llave al diccionario
datos_persona["genero"] = "M"

"""
r -> Leer
w -> Escribir
"""
#Abrir el fichero con permisos de escritura
with open("datos.json", "w") as archivo:
    #Escribimos información en el fichero json
    json.dump(datos_persona, archivo)
    print("¡Datos guardados con éxito!")

