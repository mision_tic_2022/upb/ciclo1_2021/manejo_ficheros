#Importar biblioteca JSON
import json
from os import system

#Interfaz por consola
def menu():
    system("clear")
    carros = cargar_datos()
    opc: int = -1
    while opc != 0:
        print("1 --> Crear carro")
        print("2 --> Consultar todos los carros")
        print("3 --> Actualizar carro")
        print("4 --> Eliminar carro")
        print("5 -> Buscar carro")
        print("0 --> Salir")
        print("")
        opc = int( input("Por favor ingrese una opción: ") )
        controlador(opc, carros)

def controlador(opc: int, carros: dict):
    #Condicionales
    if opc == 1:
        #Solicitar datos
        #Crear un carro
        placa: str = input("\nPlaca: ")
        marca: str = input("Marca: ")
        color: str = input("Color: ")
        #Crear diccionario a partir de los datos
        nuevo_carro: dict = {
                "marca": marca,
                "color": color
                }
        #Crear un nuevo carro en el diccionario existente
        carros[placa.upper()] = nuevo_carro
        insertar_datos(carros)
    elif opc == 2:
        leer_datos()
    elif opc == 3:
        placa: str = input("Por favor ingrese la placa del vehiculo que desea actualizar: ")
        info_carro = buscar_carro(carros, placa)
        if len(info_carro) > 0:
            info_carro["marca"] = input("Por favor ingrese la nueva marca: ")
            info_carro["color"] = input("Por favor ingrese el nuevo color: ")
            carros[placa] = info_carro
            insertar_datos(carros)
        else:
            print("El carro con la placa ingresada no existe")
    elif opc == 4:
        placa : str = input("Por favor ingrese la placa del vehiculo que desea eliminar: ")
        info_carro = buscar_carro(carros, placa)
        if len(info_carro) > 0:
            carros.pop(placa.upper())
            insertar_datos(carros)
        else:
            print("El carro con la placa ingresada no existe")
    elif opc == 5:
        placa : str = input("Por favor ingrese la placa del vehiculo que desea eliminar: ")
        info_carro = buscar_carro(carros, placa)
        print( buscar_carro(carros, placa) )

def leer_datos():
    #Creamos diccionario vacio
    carros: dict = cargar_datos()
    for placa, datos in carros.items():
        print("-------", placa, "-------")
        marca = datos["marca"]
        print("Marca: ", marca)
        color = datos["color"]
        print("Color: ", color)
        print("")

#Función para buscar un carro
def buscar_carro(carros: dict, placa: str)->dict:
    datos_carro = {}
    #Iteramos el diccionario para buscar un vehiculo por su placa
    for clave, valor in carros.items():
        #Comparativa pasando a minúscula los caracteres
        if clave.upper() == placa.upper():
            datos_carro = valor
    return datos_carro


"""
==========Funciones manipuladoras del fichero json================
"""
#Función para cargar los datos a partir de un fichero json
def cargar_datos():
    #Creamos diccionario vacio
    carros: dict = dict()
    try:
        #Cargamos la información del json
        with open("bd_carros.json") as archivo:
            carros = json.load(archivo)
    except:
        print("Error al cargar los datos")
    return carros

#Almacena los datos en el fichero 'datos_carros.json'
def insertar_datos(carros: dict):
    with open("bd_carros.json", "w") as archivo:
        json.dump(carros, archivo)
        print("\nREGISTRO GUARDADO CON ÉXITO!\n")

menu()


"""
En unos minutos regresamos...
"""