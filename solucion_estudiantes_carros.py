import json

"""
CREAR O SOBREESCRIBIR UN ARCHIVO JSON
"""
"""
informacion_carros  = {} 
def  menu (): 
    opc = -1
    while opc != 0:
        print ( "1 -> crear carro" ) 
        print ( "2 -> consultar carro" ) 
        print ( "3 -> actualizar carro" ) 
        opc  =  int  ( input  ( "Por favor ingrese una opcion:" )) 
        controlador (opc)

def  controlador ( opc ): 
    if  opc  ==  1 : 
        informacion_carros  =  cargar () 
        placa=input(str("ingresar la placa"))
        color=input(str("ingresa el color"))
        marca=input(str("ingresa la marca"))
        nuevo_carro: dict = {
            "color": color,
            "marca":marca
        }
        informacion_carros[placa] = nuevo_carro
        guardar_datos(informacion_carros)
        print  ( informacion_carros ) 
    elif  opc  == 2 : 
        estudiantes  =  consultar () 
        print  ( estudiantes ) 

def  cargar  (): 
    informacion_carros: dict = {}
    try:
        with  open  ( "informacion_carros.json" )  as  archivo : 
            informacion_carros  =  json . load ( archivo ) 
    except:
        print("Fichero no encontrado")
    return  informacion_carros

def  consultar  (): 
    print (informacion_carros)
    return  consultar

def guardar_datos(informacion_carros: dict):
    with open("informacion_carros.json", "w") as archivo:
        json.dump(informacion_carros, archivo)
        print("carro GUARDADO CON ÉXITO!")

menu()
"""


"""
Miguel
"""
import json

def menu():
    cargar_datos()
    opc=-1
    while opc !=0:
        print("1 --> Crear carro")
        print("2 --> Consultar carros")
        print("0 --> Salir")
        opc = int( input("Por favor ingrese una opción: ") )
        if opc == 1:
            crear()
        elif opc ==2:
            mostrar_datos()
        else:
            print("gracias por su visita")

def crear():
    diccionario_carros:dict={}
    placa1: str = input("Placa del vehiculo: ")
    color1: str = input("Color del vehiculo: ")
    marca1: str = input("Marca del vehiculo: ")
    diccionario_carros: dict ={
        placa1: {
            "color": color1,
            "marca": marca1
            } 
            }
    with open("bd_carros.json", "w") as archivo:
        json.dump(diccionario_carros, archivo)
        print("CARRO GUARDADO CON ÉXITO!")



def cargar_datos():
    #Creamos diccionario vacio
    diccionario_carros: dict = dict()
    try:
        #Cargamos la información del json
        with open("bd_carros.json") as archivo:
            diccionario_carros = json.load(archivo)
    except:
        with open("bd_carros.json","w") as archivo:
            json.dump(diccionario_carros,archivo)
            print("Se creo archivo nuevo")
    return diccionario_carros

def mostrar_datos():
    diccionario_muestra:dict=cargar_datos()
    for clave,valor in diccionario_muestra.items():
        print(f"-------{clave}--------")
        color2=valor["color"]
        marca2=valor["marca"]
        print("color del vehiculo: ",color2)
        print("marca del vehiculo: ",marca2)
        print("_______________________________")

menu()